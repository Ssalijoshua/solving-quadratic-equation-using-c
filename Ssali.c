#include <stdio.h>
// FUNCTION FOR GETTING ANY NUMBER TO A CERTAIN POWER
double get_exponent(double x, double y)
{
	double z = 1;
	for (double i = 0; i < y; i = i + 1)
	{
		z = x * x;
	}
	return z;
}

// FUNCTION FOR GETTING THE SQUAREROOT

double get_sqrt(double x)
{
	double i;
	for (i = 0.001; i * i < x; i = i + 0.001);
	return i;
}

// FUNCTION TO GET THE DISCRIMINANT

double get_discriminant(double a, double b, double c)
{
	c = 4 * a * c;

	c = get_exponent(b, 2) - c;

	return c;
}

int main(void)
{

	// addresses for storing the variables entered by the user
	double a, b, c;
	printf("Enter coefficient for x^2: ");
	scanf("%lf", &a);
	if (a==0){
	printf("Not a quadratic equation. Please try again!! ");
	}
	
	else{

	printf("Enter coefficient for x^1: ");
	scanf("%lf", &b);

	printf("Enter coefficient for x^0: ");
	scanf("%lf", &c);

	/*THE QUADRATIC FORMULAR
	c=(b^2-4ac)^1/2*/

	double discriminant = get_discriminant(a, b, c);

	double j = get_sqrt(discriminant);

	// RESULTS
	if (discriminant > 0)
	{

		//-b+ or -b-
		double x = -b + j;
		double y = -b - j;

		// DIVIDING BY 2a
		// for y
		y = y / 2 * a;

		// for x
		x = x / 2 * a;
		// printf("%.2f",j);
		printf("The equation has real distinct roots which are %.2lf and %.2lf\n", x, y);
		//printf("Squareroot:%lf\n Discriminant:%lf\n%A:lf\n", j, discriminant, a);
	}
	else if (discriminant == 0.00)
	{
		double x = -b + j;
		// dividing for x
		x = x / 2 * a;
		printf("The equation has real and equal roots is %.2lf", x);
		//printf("Squareroot:%lf\n Discriminant:%lf\n%A:lf\n", j, discriminant, a);
		// printf("%.2f",j);
	}
	else
	{
		printf("Complex roots\n");
		//printf("Squareroot:%lf\n Discriminant:%lf\n%A:lf\n", j, discriminant, a);
	}
	}
	// DIVIDING BY 2a
	// for y
	// y=y/2*a;

	// for x
	// x=x/2*a;

	// printf("%f\n",c);
}